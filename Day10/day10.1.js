'use strict';

// Reverse the order of that length of elements in the list, 
// starting with the element at the current position.
// Move the current position forward by that length plus the skip size.
// Increase the skip size by one.

module.exports = function (array, lengths) {
    const lengthValues = lengths.split(',').map(l => parseInt(l));
    let currentIndex = 0;
    let skipSize = 0;

    lengthValues.forEach(length => {
        if (currentIndex > array.length) {
            currentIndex = currentIndex - array.length;
        }
        if (currentIndex + length <= array.length) {
            let reversedElements = array.splice(currentIndex, length).reverse();
            array.splice(currentIndex, 0, ...reversedElements);
        }
        else {
            let distanceToEnd = array.length - currentIndex;
            let distanceFromStart = length - distanceToEnd;

            let lastElements = array.splice(currentIndex, distanceToEnd);
            let startElements = array.splice(0, distanceFromStart);
            let reversedElements = lastElements.concat(startElements).reverse();

            let index = currentIndex;
            reversedElements.forEach(e => {
                if (index > array.length) {
                    index = 0;
                }

                array[index] = e;

                index++;
            });
        }

        currentIndex += length + skipSize;
        skipSize++;
    });

    return array[0] * array[1];
}