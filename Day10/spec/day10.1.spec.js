'use strict';

const day101 = require('../day10.1');

describe('Day10.1', function () {

    beforeEach(function () {
        this.day101 = day101;
    });

    it('should return 12 for a 5 element array with the given lengths', function () {
        let array = [0, 1, 2, 3, 4];
        let lengths = '3,4,1,5';
        expect(this.day101(array, lengths)).toEqual(12);
    });
});