'use strict';

const fs = require('fs');
const day101 = require('./day10.1');
const input = fs.readFileSync('./Day10/input.txt', 'utf8');

module.exports = function () {
    let array = getArrayOfSize(256);
    return `Day10.1: ${day101(array, input)}.`;
}

function getArrayOfSize(size) {
    let array = [];
    for (let i = 0; i < size; i++) {
        array[i] = i;
    }
    return array;
}