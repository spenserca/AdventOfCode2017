'use strict';

const puzzleInput = 312051;

// 17  16  15  14  13  ...
// 18   5   4   3  12  ...
// 19   6   1   2  11  ...
// 20   7   8   9  10  ...
// 21  22  23  24  25  ...

module.exports = function (input) {
    const number = input ? input : puzzleInput;
    const cornerValue = priceIsRightOddSquare(number);

    let numberOfSteps = Math.sqrt(cornerValue);

    return numberOfSteps; // 311364
};

function priceIsRightOddSquare(number) {
    let i = 1;

    while ((i * i) < number) {
        i += 2;
    }

    return i;
};

// Calculate the nearest odd perfect square n2 from your input 
// and you have a number that is n-1 Manhattan distance away 
// from the center (the bottom right corner). Then count the 
// distance from your input.