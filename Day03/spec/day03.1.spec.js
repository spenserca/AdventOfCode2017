'use strict';

xdescribe('Challenges/day03.1', function () {

    beforeEach(function () {
        this.day031 = require('../day03.1');
    });

    it('should return 0 for 1', function () {
        expect(this.day031).toEqual(0);
    });

    it('should return 3 for 12', function () {
        expect(this.day031(12)).toEqual(3);
    });

    it('should return 2 for 23', function () {
        expect(this.day031(23)).toEqual(2);
    });


    it('should return 31 for 1024', function () {
        expect(this.day031(1024)).toEqual(31);
    });
});