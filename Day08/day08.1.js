'use strict';

module.exports = function (input) {
    const instructions = input.split('\n').
        map((l) => l.trim());

    let registers = [];

    let parsedInstructions = instructions.map((instruction) => {
        const instructionPieces = instruction.match(/^([\w]+)\s([\w]+)\s([-?\d]+)\s([if]+)\s([\w]+)\s([<>!=]+)\s([-?\d]+)$/i);

        let register = {
            name: instructionPieces[1],
            value: 0
        }

        if (!registers.find(r => r.name === instructionPieces[1])) {
            registers.push(register);
        }

        return {
            registerToModify: instructionPieces[1],
            changeType: instructionPieces[2], // inc or dec
            changeByValue: parseInt(instructionPieces[3]),
            registerToEvaluate: instructionPieces[5],
            operand: instructionPieces[6],
            compareTo: parseInt(instructionPieces[7])
        };
    });

    parsedInstructions.forEach((instruction) => {
        let comparisonValue = registers.find(r => r.name === instruction.registerToEvaluate).value;
        let isValidToProcess = false;

        switch (instruction.operand) {
            case '==':
                isValidToProcess = (comparisonValue == instruction.compareTo);
                break;
            case '<=':
                isValidToProcess = (comparisonValue <= instruction.compareTo);
                break;
            case '>=':
                isValidToProcess = (comparisonValue >= instruction.compareTo);
                break;
            case '>':
                isValidToProcess = (comparisonValue > instruction.compareTo);
                break;
            case '<':
                isValidToProcess = (comparisonValue < instruction.compareTo);
                break;
            case '!=':
                isValidToProcess = (comparisonValue != instruction.compareTo);
                break;
        }

        if (isValidToProcess) {
            instruction.changeType === 'inc'
                ? registers.find(r => r.name === instruction.registerToModify).value += instruction.changeByValue
                : registers.find(r => r.name === instruction.registerToModify).value -= instruction.changeByValue;
        }
    });

    let values = registers.map(register => {
        return register.value;
    });

    return Math.max(...values);
}