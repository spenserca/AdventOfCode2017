'use strict';

const day082 = require('../day08.2');

// --- Part Two ---
// To be safe, the CPU also needs to know the highest 
// value held in any register during this process so 
// that it can decide how much memory to allocate to 
// these operations. For example, in the above 
// instructions, the highest value ever held was 10 
// (in register c after the third instruction was evaluated).

describe('Challenges/Day08.2', function () {

    beforeEach(function () {
        this.day082 = day082;
    });

    it('should return 10 for the given input', function () {
        const instructions =
            `b inc 5 if a > 1
        a inc 1 if b < 5
        c dec -10 if a >= 1
        c inc -20 if c == 10`
        expect(this.day082(instructions)).toEqual(10);
    });
});