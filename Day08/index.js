'use strict';

const fs = require('fs');
const day081 = require('./day08.1');
const day082 = require('./day08.2');
const input = fs.readFileSync('./Day08/input.txt', 'utf8');

module.exports = function () {
    return `Day08.1: ${day081(input)}. Day08.2: ${day082(input)}.`;
}