'use strict';

module.exports = function (input) {
    let nums = input;
    let firstNums = nums.substring(0, nums.length / 2);
    let secondNums = nums.substring(nums.length / 2);
    let sum = 0;

    for (var i = 0; i < firstNums.length; i++) {
        sum += charactersMatch(firstNums.charAt(i), secondNums.charAt(i));
    }

    return sum;
};

function charactersMatch(first, second) {
    return (first === second)
        ? parseInt(first) + parseInt(second)
        : 0;
};