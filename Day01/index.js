'use strict';

const fs = require('fs');
const input = fs.readFileSync('./Day01/input.txt', 'utf8');
const day011 = require('./day01.1');
const day012 = require('./day01.2');

module.exports = function () {
    let day01Out = day011(input);
    let day02Out = day012(input);

    return `Day01.1: ${day01Out}. Day01.2: ${day02Out}.`;
}