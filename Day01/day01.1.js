'use strict';

module.exports = function (input) {
    return input.split('')
        .reduce((sum, current, currentIndex, array) => {
            if (currentIndex === array.length - 1) {
                sum += array[0] === array[currentIndex] ? parseInt(array[currentIndex]) : 0;
                return sum;
            }

            sum += array[currentIndex] === array[currentIndex + 1] ? parseInt(array[currentIndex]) : 0;
            return sum;
        }, 0);
};