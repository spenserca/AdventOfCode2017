'use strict';

const Promise = require('bluebird');

// 1122 produces a sum of 3 (1 + 2) because the first digit (1) matches the second digit and the third digit (2) matches the fourth digit.
// 1111 produces 4 because each digit (all 1) matches the next.
// 1234 produces 0 because no digit matches the next.
// 91212129 produces 9 because the only digit that matches the next one is the last digit, 9.

describe('Challenges/day01.1', function () {

    beforeEach(function () {
        this.day01 = require('../day01.1.js');
    });

    it('should return 3 for 1122', function () {
        expect(this.day01('1122')).toEqual(3);
    });

    it('should return 4 for 1111', function () {
        expect(this.day01('1111')).toEqual(4);
    });

    it('should return 0 for 1234', function () {
        expect(this.day01('1234')).toEqual(0);
    });

    it('should return 9 for 91212129', function () {
        expect(this.day01('91212129')).toEqual(9);
    });
});