'use strict';

const day161 = require('../day16.1');
const programs = 'abcde';
const dance = 's1,x3/4,pe/b';

describe('Day16.1', function () {
    it('should return eabcd for the given dance', function () {
        expect(day161(programs, 's1')).toEqual('eabcd');
    });

    it('should return abced for the given dance', function () {
        expect(day161(programs, 'x3/4')).toEqual('abced');
    });

    it('should return abced for the given dance', function () {
        expect(day161(programs, 'x4/3')).toEqual('abced');
    });

    it('should return aecdb for the given dance', function () {
        expect(day161(programs, 'pe/b')).toEqual('aecdb');
    });

    it('should return aecdb for the given dance', function () {
        expect(day161(programs, 'pb/e')).toEqual('aecdb');
    });

    it('should return baedc for the given dance', function () {
        expect(day161(programs, dance)).toEqual('baedc');
    });
});