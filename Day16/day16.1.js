'use strict';

module.exports = (programs, dance) => {
    let p = programs.split('');
    const output = dance.split(',')
        .map(dm => {
            if (isSpin(dm)) {
                var numberOfProgramsToMove = parseInt(dm.match(/[0-9]+/g));
                var programsToMove = [];
                for (var i = 0; i < numberOfProgramsToMove; i++) {
                    programsToMove.push(p.pop());
                }

                return programsToMove.concat(p);
            }
            else if (isExchange(dm)) {
                var indicesToSwap = dm.match(/([0-9]+)/g)
                    .map(v => parseInt(v));

                var firstValue = p[indicesToSwap[0]];
                p[indicesToSwap[0]] = p[indicesToSwap[1]];
                p[indicesToSwap[1]] = firstValue;

                return p;
            }
            else if (isPartnerSwap(dm)) {
                dm = dm.substr(1);
                var partnersToSwap = dm.match(/([a-zA-Z]+)/g);

                var firstIndex = p.indexOf(partnersToSwap[0]);
                var secondIndex = p.indexOf(partnersToSwap[1]);
                var firstValue = p[firstIndex];
                p[firstIndex] = p[secondIndex]
                p[secondIndex] = firstValue;

                return p;
            }
        });

    return output.join('').replace(/[,]/g, '');
}

function isSpin(danceMove) {
    return danceMove.startsWith('s');
}

function isExchange(danceMove) {
    return danceMove.startsWith('x');
}

function isPartnerSwap(danceMove) {
    return danceMove.startsWith('p');
}