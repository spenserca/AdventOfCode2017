'use strict';

const nodes = {};

module.exports = function (input) {
    let differential = 0;
    const programs = input.split('\n').map(x => x.trim());

    for (let i = 0; i < programs.length; i++) {
        const programArray = programs[i].match(/^([\w]+)\s\((\d+)\)(?:\s->\s)?(.*)$/i); // Use regex to parse the program details

        const program = {
            name: programArray[1],
            weight: parseInt(programArray[2]),
            parent: null,
            children: programArray[3] ? programArray[3].split(', ') : []
        };

        nodes[program.name] = program;
    }

    const keys = Object.keys(nodes);

    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];

        // for each child of the key
        for (let j = 0; j < nodes[key].children.length; j++) {
            const child = nodes[key].children[j];

            // set the child on the parent node
            nodes[key].children[j] = nodes[child];
            // set the parent on the child node
            nodes[child].parent = nodes[key];
        }
    }

    const getWeight = (node) => {
        return node.weight + node.children
            .map(getWeight)
            .reduce((sum, current) => sum + current, 0);
    };

    const traverse = (node, difference = 0) => {
        const children = node.children.map(child => {
            return {
                name: child.name,
                weight: getWeight(child)
            };
        });

        const unbalanced = children.find(child => {
            return children.filter(c => c.weight === child.weight).length === 1;
        });

        if (!unbalanced) {
            differential = nodes[node.name].weight - difference;
        }
        else {
            const balanced = children
                .find(child => children.filter(c => c.weight === child.weight).length > 1);

            traverse(nodes[unbalanced.name], unbalanced.weight - balanced.weight);
        }
    };

    const rootNode = Object.values(nodes).find(node => node.parent === null);

    traverse(rootNode);

    return differential;
}