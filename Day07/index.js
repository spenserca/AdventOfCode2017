'use strict';

const fs = require('fs');
const day071 = require('./day07.1');
const day072 = require('./day07.2');
const input = fs.readFileSync('./Day07/input.txt', 'utf8');

module.exports = function () {
    return `Day07.1: ${day071(input)}. Day07.2: ${day072(input)}.`;
}