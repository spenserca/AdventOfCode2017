'use strict';

const day071 = require('../day07.1');

describe('Challenges/day07.1', function () {

    beforeEach(function () {
        this.day071 = day071;
    });

    it('should return tknk for provided input', function () {
        let input = `pbga (66)
            xhth (57)
            ebii (61)
            havc (66)
            ktlj (57)
            fwft (72) -> ktlj, cntj, xhth
            qoyq (66)
            padx (45) -> pbga, havc, qoyq
            tknk (41) -> ugml, padx, fwft
            jptl (61)
            ugml (68) -> gyxo, ebii, jptl
            gyxo (61)
            cntj (57)`;
        expect(this.day071(input)).toEqual('tknk');
    });
});