'use strict';

describe('Challenges/day07.2', function () {

    beforeEach(function () {
        this.day072 = require('../day07.2');
    });

    it('should return 8 for provided input', function () {
        let input = `pbga (66)
            xhth (57)
            ebii (61)
            havc (66)
            ktlj (57)
            fwft (72) -> ktlj, cntj, xhth
            qoyq (66)
            padx (45) -> pbga, havc, qoyq
            tknk (41) -> ugml, padx, fwft
            jptl (61)
            ugml (68) -> gyxo, ebii, jptl
            gyxo (61)
            cntj (57)`;
        expect(this.day072(input)).toEqual(60);
    });
})