'use strict';

// find all programs that are holding other programs
// the base program will be the one that holds other programs
// but doesn't exist in the programs that other programs are holding

module.exports = function (input) {
    const programs = input.split('\n').map(x => x.trim());

    let nodes = {};

    for (let i = 0; i < programs.length; i++) {
        const programArray = programs[i].match(/^([\w]+)\s\((\d+)\)(?:\s->\s)?(.*)$/i); // Use regex to parse the program details

        const program = {
            name: programArray[1],
            weight: parseInt(programArray[2]),
            parent: null,
            children: programArray[3] ? programArray[3].split(', ') : []
        };

        nodes[program.name] = program;
    }

    const keys = Object.keys(nodes);

    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];

        // for each child of the key
        for (let j = 0; j < nodes[key].children.length; j++) {
            const child = nodes[key].children[j];

            // set the child on the parent node
            nodes[key].children[j] = nodes[child];
            // set the parent on the child node
            nodes[child].parent = nodes[key];
        }
    }

    const root = Object.values(nodes).find((node) => node.parent === null);

    return root.name;
}