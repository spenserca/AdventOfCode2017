'use strict';

// For example, given the following spreadsheet:

// 5 9 2 8
// 9 4 7 3
// 3 8 6 5
// In the first row, the only two numbers that evenly divide are 8 and 2; the result of this division is 4.
// In the second row, the two numbers are 9 and 3; the result is 3.
// In the third row, the result is 2.
// In this example, the sum of the results would be 4 + 3 + 2 = 9.

describe('Challenges/day02.2', function () {

    beforeEach(function () {
        this.day02 = require('../day02.2');
    });

    it('should return 9 for the given spreadsheet', function () {
        this.spreadsheet =
            `5\t9\t2\t8
            9\t4\t7\t3
            3\t8\t6\t5`;
        expect(this.day02(this.spreadsheet)).toEqual(9);
    });
});