'use strict';

// For example, given the following spreadsheet:

// 5 1 9 5
// 7 5 3
// 2 4 6 8
// The first row's largest and smallest values are 9 and 1, and their difference is 8.
// The second row's largest and smallest values are 7 and 3, and their difference is 4.
// The third row's difference is 6.
// In this example, the spreadsheet's checksum would be 8 + 4 + 6 = 18.

// What is the checksum for the spreadsheet in your puzzle input?

describe('Challenges/day02.1', function () {

    beforeEach(function () {
        this.day02 = require('../day02.1');
    });

    it('should return 18 for the given spreadsheet', function () {
        this.spreadSheet =
            `5\t1\t9\t5
            7\t5\t3
            2\t4\t6\t8`;
        expect(this.day02(this.spreadSheet)).toEqual(18);
    });
});