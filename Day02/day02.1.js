'use strict'

module.exports = function (input) {
    const spreadsheet = input.split('\n').map(r => r.trim());
    let checkSum = 0;

    spreadsheet.map(row => {
        let splitRow = row.split('\t');
        splitRow.sort(leastToGreatest);
        checkSum += getDifferenceForRow(splitRow);
    });

    return checkSum;
};

function leastToGreatest(first, second) {
    if (first < second) {
        return -1;
    }
    if (first > second) {
        return 1;
    }
    return 0;
}

function getDifferenceForRow(row) {
    return row[row.length - 1] - row[0];
}