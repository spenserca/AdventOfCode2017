'use strict';

module.exports = function (input) {
    const spreadsheet = input.split('\n').map(r => r.trim());
    let sum = 0;

    spreadsheet.map(row => {
        let splitRow = row.split('\t');
        splitRow.sort(greatestToLeast);
        for (var i = 0; i < splitRow.length - 1; i++) {
            let initialValue = splitRow.shift();
            splitRow.map(value => {
                sum += isEvenlyDivided(initialValue, value)
                    ? initialValue / value
                    : 0;
            });
        }
    });

    return sum;
};

function isEvenlyDivided(first, second) {
    return first % second === 0;
}

function greatestToLeast(first, second) {
    if (first < second) {
        return 1;
    }
    if (first > second) {
        return -1;
    }
    return 0;
};