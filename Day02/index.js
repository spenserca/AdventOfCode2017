'use strict';

const fs = require('fs');
const input = fs.readFileSync('./Day02/input.txt', 'utf8');
const day021 = require('./day02.1');
const day022 = require('./day02.2');

module.exports = function () {
    return `Day02.1: ${day021(input)}. Day02.2: ${day022(input)}.`;
}