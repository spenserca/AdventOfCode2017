'use strict';

const day01 = require('./Day01/index');
const day02 = require('./Day02/index');
// TODO: day03
const day04 = require('./Day04/index');
const day05 = require('./Day05/index');
const day06 = require('./Day06/index');
const day07 = require('./Day07/index');
const day08 = require('./Day08/index');
const day09 = require('./Day09/index');
const day10 = require('./Day10/index');

console.log(day01());
console.log(day02());
console.log(day04());
console.log(day05());
console.log(day06());
console.log(day07());
console.log(day08());
console.log(day09());
console.log(day10());

process.exit();