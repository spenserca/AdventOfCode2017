'use strict';

const fs = require('fs');
const day061 = require('./day06.1');
const day062 = require('./day06.2');
const input = fs.readFileSync('./Day06/input.txt', 'utf8');

module.exports = function () {
    return `Day06.1: ${day061(input)}. Day06.2: ${day062(input)}.`;
}