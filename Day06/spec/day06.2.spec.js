'use strict';

const day062 = require('../day06.2');

describe('Challenges/day06.2', function () {

    beforeEach(function () {
        this.day062 = day062;
    });

    it('should return 4 for 0, 2, 7, and 0 blocks', function () {
        expect(this.day062('0 2 7 0')).toEqual(4);
    });
});