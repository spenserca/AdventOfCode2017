'use strict';

module.exports = function (input) {
    const banks = input.split(' ').map(v => parseInt(v));
    let currentBankLayout = banks;
    let bankLayouts = [];

    while (!bankLayouts.includes(currentBankLayout.join(':'))) {
        bankLayouts.push(currentBankLayout.join(':'));
        let currentIndex = currentBankLayout.indexOf(Math.max(...currentBankLayout));
        let valueToDistribute = currentBankLayout[currentIndex];
        currentBankLayout[currentIndex] = 0;
        currentIndex += 1;

        for (let i = valueToDistribute; i > 0; i--) {
            if (currentIndex === currentBankLayout.length) {
                currentIndex = 0;
            }

            currentBankLayout[currentIndex]++;
            currentIndex++;
        }
    }

    return bankLayouts.length;
}