'use strict';

const day042 = require('../day04.2');

// For example:

// abcde fghij is a valid passphrase.
// abcde xyz ecdab is not valid - the letters from the third word can be rearranged to form the first word.
// a ab abc abd abf abj is a valid passphrase, because all letters need to be used when forming another word.
// iiii oiii ooii oooi oooo is valid.
// oiii ioii iioi iiio is not valid - any of these words can be rearranged to form any other word.
// Under this new system policy, how many passphrases are valid?

describe('Challenges/day04.2', function () {

    beforeEach(function () {
        this.day042 = day042;
    });

    it('should return 1 for abcde fghij', function () {
        expect(this.day042('abcde fghij')).toEqual(1);
    });

    it('should return 0 for abcde xyz ecdab', function () {
        expect(this.day042('abcde xyz ecdab')).toEqual(0);
    });

    it('should return 1 for a ab abc abd abf abj', function () {
        expect(this.day042('a ab abc abd abf abj')).toEqual(1);
    });

    it('should return 1 for iiii oiii ooii oooi oooo', function () {
        expect(this.day042('iiii oiii ooii oooi oooo')).toEqual(1);
    });

    it('should return 0 for oiii ioii iioi iiio', function () {
        expect(this.day042('oiii ioii iioi iiio')).toEqual(0);
    });
});