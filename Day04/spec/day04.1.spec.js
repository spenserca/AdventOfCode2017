'use strict';

// To ensure security, a valid passphrase must contain no duplicate words.

// For example:

// aa bb cc dd ee is valid.
// aa bb cc dd aa is not valid - the word aa appears more than once.
// aa bb cc dd aaa is valid - aa and aaa count as different words.

describe('Challenges/day04.1', function () {

    beforeEach(function () {
        this.day041 = require('../day04.1');
    });

    it('should return 1 for aa bb cc dd ee', function () {
        expect(this.day041('aa bb cc dd ee')).toEqual(1);
    });

    it('should return 0 for aa bb cc dd aa', function () {
        expect(this.day041('aa bb cc dd aa')).toEqual(0);
    });

    it('should return 1 for aa bb cc dd aaa', function () {
        expect(this.day041('aa bb cc dd aaa')).toEqual(1);
    });
});