'use strict';

const fs = require('fs');
const input = fs.readFileSync('./Day04/input.txt', 'utf8');
const day041 = require('./day04.1');
const day042 = require('./day04.2');

module.exports = function () {
    return `Day04.1: ${day041(input)}. Day04.2: ${day042(input)}.`;
}