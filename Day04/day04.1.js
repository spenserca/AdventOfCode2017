'use strict';

module.exports = function (input) {
    let passphrases = input.split('\n').map(r => r.trim());
    let validCount = 0;

    passphrases.map(passphrase => {
        isValidPassphrase(passphrase)
            ? validCount++
            : 0;
    });

    return validCount;
};

function isValidPassphrase(passphrase) {
    return passphrase.split(' ').length === new Set(passphrase.split(' ')).size;
};