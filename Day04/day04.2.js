'use strict';

module.exports = function (input) {
    let passphrases = input.split('\n').map(r => r.trim());
    let validCount = 0;

    passphrases.map(passphrase => {
        isValidPassphrase(passphrase)
            ? validCount++
            : 0;
    });

    return validCount;
};

function isValidPassphrase(passphrase) {
    let values = passphrase.split(' ');
    let sortedValues = values.map(value => {
        return value.split('').sort().toString().replace(',', '');
    });

    if (values.length !== new Set(sortedValues).size) { // if there are duplicates then anagram
        return false;
    }
    return true;
};