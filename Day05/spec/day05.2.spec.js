'use strict';

const day052 = require('../day05.2');

// Now, the jumps are even stranger: after each jump, if the offset was three or more, 
// instead decrease it by 1. Otherwise, increase it by 1 as before.

// Using this rule with the above example, the process now takes 10 steps, 
// and the offset values after finding the exit are left as 2 3 2 3 -1.

// How many steps does it now take to reach the exit?

describe('Challenges/day05.2', function () {

    beforeEach(function () {
        this.day052 = day052;
    });

    it('should return 10 for 0, 3, 0, 1, -3', function () {
        let input = '0\r\n3\r\n0\r\n1\r\n-3';
        expect(this.day052(input)).toEqual(10);
    });
});