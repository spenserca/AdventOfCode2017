'use strict';

const day051 = require('../day05.1');

describe('Challenges/day05.1', function () {

    beforeEach(function () {
        this.day051 = day051;
    });

    it('should return 5 for 0, 3, 0, 1, -3', function () {
        let input = '0\r\n3\r\n0\r\n1\r\n-3';
        expect(this.day051(input)).toEqual(5);
    });
});