'use strict';

const fs = require('fs');
const input = fs.readFileSync('./Day05/input.txt', 'utf8');
const day051 = require('./day05.1');
const day052 = require('./day05.2');

module.exports = function () {
    return `Day05.1: ${day051(input)}. Day05.2: ${day052(input)}.`;
}