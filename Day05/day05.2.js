'use strict';

module.exports = function (input) {
    const offsets = input.split('\r\n').map(v => parseInt(v));

    let currentIndex = 0;
    let totalSteps = 0;

    while (currentIndex < offsets.length) {
        const steps = offsets[currentIndex];
        steps >= 3
            ? offsets[currentIndex]--
            : offsets[currentIndex]++;

        totalSteps++;
        currentIndex += steps;
    }

    return totalSteps;
}