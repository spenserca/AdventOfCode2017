'use strict';

const day092 = require('../day09.2');

// --- Part Two ---
// Now, you're ready to remove the garbage.

// To prove you've removed it, you need to count all of the characters within the 
// garbage. The leading and trailing < and > don't count, nor do any canceled 
// characters or the ! doing the canceling.

// <>, 0 characters.
// <random characters>, 17 characters.
// <<<<>, 3 characters.
// <{!>}>, 2 characters.
// <!!>, 0 characters.
// <!!!>>, 0 characters.
// <{o"i!a,<{i<a>, 10 characters.
// How many non-canceled characters are within the garbage in your puzzle input?

describe('Day09.2', function () {

    beforeEach(function () {
        this.day092 = day092;
    });

    it('should return 0 for <>', function () {
        expect(this.day092('<>')).toEqual(0);
    });

    it('should return 17 for <random characters>', function () {
        expect(this.day092('<random characters>')).toEqual(17);
    });

    it('should return 3 for <<<<>', function () {
        expect(this.day092('<<<<>')).toEqual(3);
    });

    it('should return 2 for <{!>}>', function () {
        expect(this.day092('<{!>}>')).toEqual(2);
    });

    it('should return 0 for <!!>', function () {
        expect(this.day092('<!!>')).toEqual(0);
    });

    it('should return 0 for <!!!>>', function () {
        expect(this.day092('<!!!>>')).toEqual(0);
    });

    it('should return 10 for <{o"i!a,<{i<a>', function () {
        expect(this.day092('<{o"i!a,<{i<a>')).toEqual(10);
    });
})