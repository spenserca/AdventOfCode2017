'use strict';

module.exports = function (input) {
    const stream = input.replace(/(![\S])/g, '');
    let count = 0;
    let isGarbage = false;

    for (let i = 0; i < stream.length; i++) {
        if (isGarbage && stream[i] !== '>') {
            count++;
        }

        if (stream[i] === '<') {
            isGarbage = true;
        }
        if (stream[i] === '>') {
            isGarbage = false;
        }
    }

    return count;
}