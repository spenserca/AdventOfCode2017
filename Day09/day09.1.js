'use strict';

module.exports = function (input) {
    const stream = input;
    let score = 0;
    let previousScore = 0;
    let count = 0;
    let ignore = false;
    let isGarbage = false;

    for (let i = 0; i < stream.length; i++) {
        if (ignore) {
            ignore = false;
            continue;
        }

        if (stream[i] === '!') {
            ignore = true;
        }
        if (stream[i] === '<') {
            isGarbage = true;
        }
        if (stream[i] === '>') {
            isGarbage = false;
        }
        if (stream[i] === '{' && !isGarbage) {
            score += previousScore++ + 1;
        }
        if (stream[i] === '}' && !isGarbage) {
            previousScore -= 1;
        }
    }

    return score;
}