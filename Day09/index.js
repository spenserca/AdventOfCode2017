'use strict';

const fs = require('fs');
const day091 = require('./day09.1');
const day092 = require('./day09.2');
const input = fs.readFileSync('./Day09/input.txt', 'utf8');

module.exports = function () {
    return `Day09.1: ${day091(input)}. Day09.2: ${day092(input)}.`;
}